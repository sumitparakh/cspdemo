<?php
    header('Content-Type: text/event-stream');
    header('Cache-Control: no-cache'); // recommended to prevent caching of event data.

    echo "server time: " . date("h:i:s") . PHP_EOL;
    echo PHP_EOL;

    flush();
?>