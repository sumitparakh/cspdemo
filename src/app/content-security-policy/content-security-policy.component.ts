import {
  Component, OnInit, ViewEncapsulation, ElementRef,
  Inject, AfterViewInit
} from '@angular/core';
import { CSPType } from '../csputil';
import { Meta, DOCUMENT, DomSanitizer } from '@angular/platform-browser';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { isFulfilled } from 'q';

@Component({
  selector: 'app-content-security-policy',
  templateUrl: './content-security-policy.component.html',
  styleUrls: ['./content-security-policy.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class ContentSecurityPolicyComponent implements OnInit, AfterViewInit {

  type = 'info';
  script: any = {
    script_src: { rules: [], values: [] },
    default_src: { rules: [], values: [] },
    style_src: { rules: [], values: [] },
    object_src: { rules: [], values: [] },
    img_src: { rules: [], values: [] },
    media_src: { rules: [], values: [] },
    font_src: { rules: [], values: [] },
    child_src: { rules: [], values: [] },
    connect_src: { rules: [], values: [] }
  };

  policies: Array<CSPType> = [];

  cspmeta: '';

  selectedFaqId: 'default_src';

  private _success = new Subject<string>();

  successMessage: string;

  private attributesInQuotes = [
    'self',
    'unsafe-inline',
    'unsafe-eval',
    'none'
  ];

  constructor(
    @Inject(SESSION_STORAGE) private storage: StorageService,
    private elementRef: ElementRef,
    private meta: Meta,
    @Inject(DOCUMENT) private document: any,
    private route: ActivatedRoute,
    private router: Router,
    private sanitizer: DomSanitizer) {

    const $this = this;

    this.setScript();

    route.params.subscribe(x => {
      this.selectedFaqId = x.panelid;
      $this.loadResources(x.panelid);
    });

    this.setCSPMetaTag();

    this.policies = this.storage.get('csppolicy') || [];
    if (this.policies.length === 0) {
      this.policies.push({
        name: 'default_src',
        value: '',
        label: 'Default',
        description: 'Define loading policy for all resources type in case of a resource type \
        dedicated directive is not defined (fallback).'
      });
      this.policies.push({
        name: 'script_src',
        value: '\'self\'  cdnjs.cloudflare.com',
        label: 'Script',
        description: 'Define which scripts(javascript) the protected resource can execute'
      });

      this.policies.push({
        name: 'style_src',
        value: '\'self\' \'unsafe-inline\' \'unsafe-eval\'',
        label: 'Stylesheet',
        description: 'Define which styles (CSS) the user applies to the protected resource'
      });

      this.policies.push({
        name: 'object_src',
        value: '\'self\'',
        label: 'Object',
        description: 'Defines valid sources of plugins, eg <object>, <embed> or <applet>.'
      });

      this.policies.push({
        name: 'img_src',
        value: '\'self\'',
        label: 'Images',
        description: 'Define from where the protected resource can load images'
      });

      this.policies.push({
        name: 'media_src',
        value: '\'self\'',
        label: 'Media',
        description: 'Define from where the protected resource can load video and audio.  eg HTML5 <audio>, <video> elements.'
      });

      this.policies.push({
        name: 'font_src',
        value: '\'self\'',
        label: 'Font',
        description: 'Define from where the protected resource can load fonts'
      });

      this.policies.push({
        name: 'child_src',
        value: '\'self\'',
        label: 'Child (frame,iframe etc.)',
        description: 'Defines valid sources for web workers and nested browsing contexts loaded using elements such as <frame> and <iframe>'
      });

      this.policies.push({
        name: 'connect_src',
        value: '\'self\'',
        label: 'Script URIs',
        description: 'Define which URIs the protected resource can load using script interfaces. \
        Applies to XMLHttpRequest (AJAX), WebSocket or EventSource. If not allowed the browser \
        emulates a 400 HTTP status code. '
      });

      this.storage.set('csp', this.policies);

    } else {
      this.setCSPMetaTag();
    }

  }

  loadResources(panelId: string) {
    if (panelId === 'script_src') {
      this.changeScriptSrc(panelId);
    } else if (panelId === 'style_src') {
      this.createStyleSheets(panelId);
    } else if (panelId === 'font_src') {
      this.addFonts(panelId);
    } else if (panelId === 'connect_src') {
      this.addEventSource(panelId);
    }
  }

  addEventSource(policy: string) {
    if (this.script[policy].values) {
      this.script[policy].values.map(eventsource => {
        const evtSource = new EventSource(eventsource);
        evtSource.onmessage = function (e) {
          const eventList = document.getElementById('connectSrc');
          eventList.innerHTML = 'Response from ' + eventsource + ': ' + e.data;
        };
      });
    }
  }

  setScript() {
    const scripts = this.storage.get('policyValues');
    if (scripts) {
      this.script = scripts;
    }
  }

  saveRules(policy: CSPType, event) {
    if (event.target.value) {
      this.script[policy.name].rules.push(event.target.value);
      this.storage.set('policyValues', this.script);
      event.target.value = '';
      event.target.focus();
    }
  }

  saveValues(policy: CSPType, event) {
    if (event.target.value) {
      this.script[policy.name].values.push(event.target.value);
      this.storage.set('policyValues', this.script);
      event.target.value = '';
      event.target.focus();
    }
  }

  clearStorage() {
    this.storage.remove('csp');
    this.storage.remove('policyValues');
    this.storage.remove('cspmeta');
    this.success('Local storage data successfully removed.');
  }

  onPanelChange(event: NgbPanelChangeEvent) {
    this.router.navigateByUrl('/csp/' + event.panelId);
  }

  reloadPage(rule) {
    location.reload();
  }

  setCSPMetaTag() {

    this.meta.removeTag('http-equiv="Content-Security-Policy"');

    let contents = '';
    Object.keys(this.script).map((policy, key) => {
      this.script[policy].rules = this.script[policy].rules
        .map((item) => this.attributesInQuotes.includes(item) ? '\'' + item + '\'' : item);
      const contentValue = this.script[policy].rules.join([' ']);
      if (contentValue) {
        contents += policy.replace('_', '-') + ' ' + contentValue + ';';
      }
    });

    this.meta.addTag({
      'http-equiv': 'Content-Security-Policy',
      content: contents
    });
  }

  ngOnInit() {
    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => this.successMessage = null);
  }

  ngAfterViewInit() {
    this.route.params.subscribe(value => {
      document.getElementById(value.panelid + '-header').scrollIntoView();
    });
    // console.log('router ', this.route.params.value.panelid);
    // document.getElementById(panelId + '-header').scrollIntoView();
  }

  success(message) {
    this._success.next(message);
  }

  remove(index: number, policyName: string, type: string) {
    if (index !== -1) {
      this.script[policyName][type].splice(index, 1);
      this.storage.set('policyValues', this.script);
    }
  }

  changeScriptSrc(policy: string) {
    this.script = this.storage.get('policyValues') || this.script;
    this.removeElements('script.csp');
    if (this.script[policy].values) {
      this.script[policy].values.map(script => {
        const isExternalJs = script.indexOf('http') > -1;
        const s = document.createElement('script');
        s.className = 'csp';
        s.type = 'text/javascript';
        s.src = isExternalJs ? script : ('./../../assets/js/' + (script.indexOf('.js') > -1 ? script : script + '.js'));
        this.elementRef.nativeElement.appendChild(s);
      });
    }
  }

  removeElements(element: string) {
    const styles = this.document.querySelectorAll(element);
    for (let index = 0; index < styles.length; index++) {
      styles[index].remove();
    }
  }

  createStyleSheets(policy: string) {
    this.script = this.storage.get('policyValues') || this.script;
    this.removeElements('link.csp');
    if (this.script[policy].values) {
      this.script[policy].values.map(style => {
        const isExternalCss = style.indexOf('http') > -1;
        const s = document.createElement('link');
        s.className = 'csp';
        s.rel = 'stylesheet';
        s.href = isExternalCss ? style : ('./../../assets/css/' + style);
        this.elementRef.nativeElement.appendChild(s);
      });
    }
  }

  addFonts(policy: string) {
    this.script = this.storage.get('policyValues') || this.script;
    this.removeElements('link.fonts');
    if (this.script[policy].values) {
      this.script[policy].values.map(style => {
        const isExternalCss = style.indexOf('http') > -1;
        const s = document.createElement('link');
        s.className = 'fonts';
        s.rel = 'stylesheet';
        s.href = isExternalCss ? style : ('./../../assets/fonts/' + style);
        if (isExternalCss) {
          this.elementRef.nativeElement.appendChild(s);
        } else {
          this.elementRef.nativeElement.style.fontFamily = style;
        }

        if (isExternalCss) {
          const regex = /family=([a-zA-Z]+)/gm;
          const str = style;
          let m;

          while ((m = regex.exec(str)) !== null) {

            // This is necessary to avoid infinite loops with zero-width matches

            if (m.index === regex.lastIndex) {
              regex.lastIndex++;
            }

            // The result can be accessed through the `m`-variable.

            m.forEach((match, groupIndex) => {
              if (groupIndex === 1) {
                this.elementRef.nativeElement.style.fontFamily = match;
              }
            });

          }
        }
      });
    }
  }

  getData(objectData, policy) {
    if (policy.name === 'object_src') {
      return objectData ? this.sanitizer.bypassSecurityTrustResourceUrl(
        objectData.indexOf('http') > -1 ? objectData : ('./../../assets/object/' + objectData)
      ) : '';
    }
    if (policy.name === 'img_src') {
      return objectData ? this.sanitizer.bypassSecurityTrustResourceUrl(
        objectData.indexOf('http') > -1 ? objectData : ('./../../assets/images/' + objectData)
      ) : '';
    }
    if (policy.name === 'media_src') {
      return objectData ? this.sanitizer.bypassSecurityTrustResourceUrl(
        objectData.indexOf('http') > -1 ? objectData : ('./../../assets/media/' + objectData)
      ) : '';
    }
    if (policy.name === 'child_src') {
      return objectData ? this.sanitizer.bypassSecurityTrustResourceUrl(
        objectData.indexOf('http') > -1 ? objectData : ('./../../assets/child/' + objectData)
      ) : '';
    }

  }

}
