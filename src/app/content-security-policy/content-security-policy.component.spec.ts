import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentSecurityPolicyComponent } from './content-security-policy.component';

describe('ContentSecurityPolicyComponent', () => {
  let component: ContentSecurityPolicyComponent;
  let fixture: ComponentFixture<ContentSecurityPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentSecurityPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentSecurityPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
