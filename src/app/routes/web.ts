import { AppComponent } from '../app.component';
import { ContentSecurityPolicyComponent } from '../content-security-policy/content-security-policy.component';
import { HomeComponent } from '../home/home.component';

export const ROUTES = [
    {path: '', component: HomeComponent},
    {path: 'csp/:panelid', component: ContentSecurityPolicyComponent}
];
