import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ContentSecurityPolicyComponent } from './content-security-policy/content-security-policy.component';
import { ROUTES } from './routes/web';
import { HomeComponent } from './home/home.component';
import { NgbModule, NgbAlertModule, NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { StorageServiceModule } from 'angular-webstorage-service';
@NgModule({
  declarations: [
    AppComponent,
    ContentSecurityPolicyComponent,
    HomeComponent,
  ],
  imports: [
    RouterModule.forRoot(ROUTES, {
      useHash: true
    }),
    BrowserModule,
    NgbModule.forRoot(),
    NgbAlertModule,
    NgbAccordionModule,
    FormsModule,
    StorageServiceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
